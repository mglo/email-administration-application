package com.mg.hp.emailapp;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Email {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String department;
    private int mailBoxCapacity;
    private String alternateEmail;

    private static final int DEFAULT_PASS_LENGTH = 8;
    private static final String COMPANY_SUFFIX = "matgl.com";

    //Constructor to receive the first name and last name
    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = randomPassword(DEFAULT_PASS_LENGTH);
        this.department = setDepartment();
        System.out.println("EMAIL CREATED!");
        System.out.println("Generated password is " + password);
        System.out.println("Provided department is: " + department);

        //Combine elements to generate email
        email = firstName.toLowerCase() + "." + lastName.toLowerCase() +"@" + COMPANY_SUFFIX;
        System.out.println("Email: " + email);
    }

    //ask for the department
    private String setDepartment(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Department codes:\n1 for Sales\n2 for Development\n3 for Accounting\n0 for none\nEnter department code: ");
        int departmentChoice = scanner.nextInt();
        switch (departmentChoice){
            case 1:
                return "sales";
            case 2:
                return "development";
            case 3:
                return "accounting";
            default:
                return "";
        }
    }

    //generate a random pass
    private String randomPassword(int length){
        String passwordSED = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
        char[] password = new char[length];
        for (int i = 0; i < length; i++) {
            int rand = (int) (Math.random() * passwordSED.length());
            password[i] = passwordSED.charAt(rand);
        }
        return new String(password);
    }

    //set the mailbox capacity
    public void setMailBoxCapacity(int capacity){
        this.mailBoxCapacity = capacity;
    }

    //set the alternate e-mail
    public void setAlternateEmail(String altEmail){
        this.alternateEmail = altEmail;
    }

    //change the password
    public void changePassword(String password){
        this.password = password;
    }

    public int getMailBoxCapacity() {
        return mailBoxCapacity;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Email{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", department='" + department + '\'' +
                ", mailBoxCapacity=" + mailBoxCapacity +
                ", alternateEmail='" + alternateEmail + '\'' +
                '}';
    }
}
